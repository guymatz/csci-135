// Here's a comment
/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: HW 1, Write a program that prints three items, such as the names
of your 3 best friends or favorite movies, on three separate lines

*/

#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

int main() {

    int quantity = 10;
    double price = 19.95;
    cout << "Quantity: " << setw(4) << quantity;
    cout << "Price: " << fixed << setw(8) << setprecision(2) << price;
    cout << "Price: " << fixed << setprecision(2) << price;
    cout << fixed << setprecision(3) << price;
    cout << fixed << setprecision(1) << price;

    return 0;
}


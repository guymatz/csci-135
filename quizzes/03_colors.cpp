// Here's a comment
/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: quiz 3
read a file of color data and prints to screen
 */

#include <iostream>
#include <fstream>

using namespace std;

int main() {
    ifstream fin("data.txt");
    if (fin.fail()) {
        cerr << "OOPS!";
        exit(1);
    }

    string line;
    while (getline(fin, line)) {
        cout << line << endl;
    }

    fin.close();

    return 0;
}

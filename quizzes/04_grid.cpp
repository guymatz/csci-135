// Here's  comment
/*
 * Guy Matz
 * 20200608
 * CSCI 135/6
 * Professor Minh Nguyen
 * Output a 5 wide, 6 star tall grid to the console
*/

#include <iostream>
using namespace std;

int main() {

    for (int i = 0; i < 6; i++) {
        for (int j=0; j < 5; j++) {
            cout << "*";
            if (j < 4) {
                cout << " ";
            }
        }
        cout << endl;
    }
}

/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: quiz 1
  Returns the larger of two numbers
*/

#include <iostream>
using namespace std;

int main() {
    int first, second;
    cout << "Enter the first number: ";
    cin >> first;
    cout << "Enter the second number: ";
    cin >> second;

    cout << "The larger of the two is ";
    // another comment
    if (first > second) {
        cout << first;
    } else {
        cout << second;
    }

    return 0;
}

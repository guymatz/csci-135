// Here's  comment
/*
 * Guy Matz
 * 20200608
 * CSCI 135/6
 * Professor Minh Nguyen
 * Output a 5 wide, 6 star tall grid to the console
*/

#include <iostream>
using namespace std;

int max3(int one, int two, int three);

int main() {

    cout << max3(1,2,3) << endl;
    cout << max3(-22, 18, 9) << endl;
}

int max3(int one, int two, int three) {
    if (one > two and one > three) {
        return one;
    }
    else if (two > one and two > three) {
        return two;
    }
    else {
        return three;
    }
}

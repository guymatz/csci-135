/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: quiz 2
  this print out an array with entries 1-10
*/

#include <iostream>
using namespace std;

int main() {
    
    int int_arr[10];
    for (int i=0; i < 10; i++) {
        int_arr[i] = i+1;
    }
    for (int i=0; i < 10; i++) {
        cout << int_arr[i] << endl;
    }

    return 0;
}

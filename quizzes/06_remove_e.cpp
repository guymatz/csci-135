// Here's  comment
/*
 * Guy Matz
 * 20200608
 * CSCI 135/6
 * Professor Minh Nguyen
 * Output a 5 wide, 6 star tall grid to the console
*/

#include <iostream>
using namespace std;

void remove_e(string & sentence);

int main() {

    string line = "Hello from everyone here.";
    remove_e(line);
    cout << line << endl;
}

void remove_e(string & sentence) {

    for (int i=0; i < sentence.length(); i++) {
       if (sentence[i] == 'e') {
           for (int j=i; j < sentence.length()-1; j++) {
               sentence[j] = sentence[j+1];
               sentence[j+1] = ' ';
           }
        }
    }
}

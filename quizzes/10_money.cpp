/*
  Author: Guy Matz

  Description:
    The program reads a PGM image from the file "inImage.pgm",
    and outputs a checkerboard image to "outImage.pgm"
*/


#include <iostream>
#include <cassert>
#include <cstdlib>
#include <fstream>

using namespace std;

class Money {
    public:
        int dollars;
        int cents;
};

Money add_money(Money x, Money y);

int main() {
    Money m1 = {4, 80};
    Money m2 = {3, 90};
    Money m3 = add_money(m1, m2);
    cout << m3.dollars << "." << m3.cents << endl;

    Money m4 = {2, 40};
    Money m5 = {2, 15};
    Money m6 = add_money(m4, m5);
    cout << m6.dollars << "." << m6.cents << endl;

    return 0;
}

Money add_money(Money x, Money y) {
    // Money m = new Money;
    Money m = {0,0};
    m.dollars = (x.dollars + y.dollars) + (x.cents + y.cents) / 100;
    m.cents = (x.cents + y.cents) % 100;

    return m;
}

/* 
 * Guy Matz
 * CSCI - 136
 * Ming Nguyen
 * Show ASCII for input
*/

#include <iostream>
#include <cctype>
using namespace std;

int countChar(string line, char c) {
    int ctr = 0;
    for (int i=0; i < line.length(); i++) {
        if (line[i] == c) ctr++;
    }
    return ctr;
}

int unbalanced_brackets(string input) {
    int opening_braces=0;
    opening_braces = countChar(input, '{');
    int closing_braces=0;
    closing_braces = countChar(input, '}');

    return opening_braces - closing_braces;
}


// a comment
int main() {

    cout << unbalanced_brackets("{{}}") << endl;
    cout << unbalanced_brackets("{{}") << endl;
    cout << unbalanced_brackets("{}}}}") << endl;
    cout << unbalanced_brackets("}{}{{{") << endl;

    return 0;
}


/*
  Author:   <---  Write your name here

  Description:
    implement the class Profile that can store the info about
    a user of the network.

*/


#include <iostream>

using namespace std;

class Profile {
    private:
        string username;
        string displayname;
    public:
        Profile(string uname, string dname);
        Profile();
        string getUsername();
        string getFullName();
        void setDisplayName(string dname);
};

Profile::Profile(string uname, string dname) {
    username = uname;
    displayname = dname;
}

Profile::Profile() {
    username = "";
    displayname = "";
}

string Profile::getUsername() {
    return username;
}

string Profile::getFullName() {
    return displayname + " (@" + username + ")";
}

void Profile::setDisplayName(string dname) {
    displayname = dname;
}

int main() {
    /* Part A
    Profile p1("marco", "Marco");    
    cout << p1.getUsername() << endl; // marco
    cout << p1.getFullName() << endl; // Marco (@marco)

    p1.setDisplayName("Marco Rossi"); 
    cout << p1.getUsername() << endl; // marco
    cout << p1.getFullName() << endl; // Marco Rossi (@marco)
    
    Profile p2("tarma1", "Tarma Roving");    
    cout << p2.getUsername() << endl; // tarma1
    cout << p2.getFullName() << endl; // Tarma Roving (@tarma1)
    */

}

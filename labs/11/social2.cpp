/*
  Author:   <---  Write your name here

  Description:
    implement the class Profile that can store the info about
    a user of the network.

*/

#include <iostream>
#include <cctype>

using namespace std;

class Profile {
    private:
        string username;
        string displayname;
    public:
        Profile(string uname, string dname);
        Profile();
        string getUsername();
        string getFullName();
        void setDisplayName(string dname);
};

Profile::Profile(string uname, string dname) {
    username = uname;
    displayname = dname;
}

Profile::Profile() {
    username = "";
    displayname = "";
}

string Profile::getUsername() {
    return username;
}

string Profile::getFullName() {
    return displayname + " (@" + username + ")";
}

void Profile::setDisplayName(string dname) {
    displayname = dname;
}

class Network {
private:
    static const int MAX_USERS=20;
    int numUsers;
    Profile profiles[MAX_USERS];
    int findId(string uname);
public:
    Network();
    bool addUser(string uname, string dname);
};

Network::Network() {
    numUsers = 0;
}

bool Network::addUser(string uname, string dname) {
    if (numUsers == 20) {
        return false;
    }
    else if (uname.size() == 0) {
        return false;
    }
    for (int i=0; i < uname.size(); i++) {
        if (! isalnum(uname[i])) {
            return false;
        }
    }
    for (int i=0; i < numUsers; i++) {
        if (profiles[i].getUsername() == uname) {
            return false;
        }
    }
    // comments
    Profile p = Profile(uname, dname);
    profiles[numUsers] = p;
    numUsers++;
    return true;
}

int main() {
  Network nw;
  cout << nw.addUser("mario", "Mario") << endl;     // true (1)
  cout << nw.addUser("luigi", "Luigi") << endl;     // true (1)

  cout << nw.addUser("mario", "Mario2") << endl;    // false (0)
  cout << nw.addUser("mario 2", "Mario2") << endl;  // false (0)
  cout << nw.addUser("mario-2", "Mario2") << endl;  // false (0)

  for(int i = 2; i < 20; i++)
      cout << nw.addUser("mario" + to_string(i), 
                 "Mario" + to_string(i)) << endl;   // true (1)

  cout << nw.addUser("yoshi", "Yoshi") << endl;     // false (0)
}

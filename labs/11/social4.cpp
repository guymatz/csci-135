/*
  Author:   <---  Write your name here

  Description:
    implement the class Profile that can store the info about
    a user of the network.

*/

#include <iostream>
#include <cctype>

using namespace std;

struct Post{
    string username;
    string message;
};

class Profile {
    // 1
    private:
        string username;
        string displayname;
    // 2
    public:
        Profile(string uname, string dname);
        Profile();
        string getUsername();
        string getFullName();
        void setDisplayName(string dname);
};

Profile::Profile(string uname, string dname) {
    username = uname;
    displayname = dname;
}

Profile::Profile() {
    username = "";
    displayname = "";
}

string Profile::getUsername() {
    // 4
    return username;
}

string Profile::getFullName() {
    // 3
    return displayname + " (@" + username + ")";
}

void Profile::setDisplayName(string dname) {
    // 5
    displayname = dname;
}

class Network {
    // 6
private:
    static const int MAX_POSTS=100;
    static const int MAX_USERS=20;
    int numUsers;
    int numPosts;
    Profile profiles[MAX_USERS];
    Post posts[MAX_POSTS];
    int findId(string uname);
    bool following[MAX_USERS][MAX_USERS];
    bool x_following_y(string user_1, string user_2);
public:
    Network();
    bool addUser(string uname, string dname);
    bool follow(string user_1, string user_2);
    void printDot();
    bool writePost(string uname, string msg);
    bool printTimeline(string uname);
};

Network::Network() {
    numUsers = 0;
    numPosts = 0;
    for (int i=0; i < MAX_USERS; i++) {
        for (int j=0; j < MAX_USERS; j++) {
            following[i][j] = false;
        }
    }
}

bool Network::writePost(string uname, string msg) {
    if ((findId(uname) >= 0) and (numPosts < MAX_POSTS)) {
        posts[numPosts] = {uname, msg};
        numPosts++;
        //cout << "numPosts: " << numPosts << ", " << msg << endl;
        return true;
    }
    return false;
}

bool Network::x_following_y(string user_1, string user_2) {
    int user_1_id = findId(user_1);
    int user_2_id = findId(user_2);
    //cout << "1 " << user_1_id << " " << user_1 << endl;
    //cout << "2 " << user_2_id << " " << user_2 << endl;
    if (following[user_1_id][user_2_id]) {
        //cout << user_1 << " is following " << user_2 << endl;
        return true;
    }
    else {
        return false;
    }
}

bool Network::printTimeline(string uname) {
    //cout << "Total posts: " << numPosts << endl;
    //cout << "Total posts: " << numPosts << endl;
    //cout << "Total posts: " << numPosts << endl;
    //cout << "Total posts: " << numPosts << endl;
    //cout << "Total posts: " << numPosts << endl;
    string poster = "";
    int userid = -1;
    for (int i=numPosts; i >= 0; i--) {
        poster = posts[i].username;
        if (poster == uname) {
            userid = findId(uname);
        }
        else if (x_following_y(uname, poster)) {
            userid = findId(poster);
        }
        else {
            continue;
        }
        Profile user = profiles[userid];
        cout << user.getFullName() << ": " << posts[i].message << endl;
    }
    return true;
}

int Network::findId(string uname) {
    for (int i=0; i < numUsers; i++) {
        if (profiles[i].getUsername() == uname) {
            return i;
        }
    }
    return -1;
}

bool Network::follow(string user_1, string user_2) {
    int row = findId(user_1);
    int col = findId(user_2);

    following[row][col] = true;
    return true;
}

void Network::printDot() {
    cout << "digraph {" << endl;
    for (int i=0; i < numUsers; i++) {
        cout << "  \"@" << profiles[i].getUsername() << "\"" << endl;
    }
    for (int i=0; i < numUsers; i++) {
        for (int j=0; j < numUsers; j++) {
            if (following[i][j]) {
                cout << "  \"@" << profiles[i].getUsername() << "\" -> \"@" << profiles[j].getUsername() << "\"" << endl;
            }
        }
    }
    cout << "}" << endl;
}

bool Network::addUser(string uname, string dname) {
    if (numUsers == 20) {
        return false;
    }
    else if (uname.size() == 0) {
        return false;
    }
    for (int i=0; i < uname.size(); i++) {
        if (! isalnum(uname[i])) {
            return false;
        }
    }
    for (int i=0; i < numUsers; i++) {
        if (profiles[i].getUsername() == uname) {
            return false;
        }
    }
    // comments
    Profile p = Profile(uname, dname);
    profiles[numUsers] = p;
    numUsers++;
    return true;
}

int main() {
  Network nw;
  // add three users
  nw.addUser("mario", "Mario");
  nw.addUser("luigi", "Luigi");
  nw.addUser("yoshi", "Yoshi");
   
  nw.follow("mario", "luigi");
  nw.follow("luigi", "mario");
  nw.follow("luigi", "yoshi");
  nw.follow("yoshi", "mario");

  // write some posts
  nw.writePost("mario", "It's a-me, Mario!");
  nw.writePost("luigi", "Hey hey!");
  nw.writePost("mario", "Hi Luigi!");
  nw.writePost("yoshi", "Test 1");
  nw.writePost("yoshi", "Test 2");
  nw.writePost("luigi", "I just hope this crazy plan of yours works!");
  nw.writePost("mario", "My crazy plans always work!");
  nw.writePost("yoshi", "Test 3");
  nw.writePost("yoshi", "Test 4");
  nw.writePost("yoshi", "Test 5");

  cout << endl;
  cout << "======= Mario's timeline =======" << endl;
  nw.printTimeline("mario");
  cout << endl;

  cout << "======= Yoshi's timeline =======" << endl;
  nw.printTimeline("yoshi");
  cout << endl;
}


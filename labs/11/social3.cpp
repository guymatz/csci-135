/*
  Author:   <---  Write your name here

  Description:
    implement the class Profile that can store the info about
    a user of the network.

*/

#include <iostream>
#include <cctype>

using namespace std;

class Profile {
    // 1
    private:
        string username;
        string displayname;
    // 2
    public:
        Profile(string uname, string dname);
        Profile();
        string getUsername();
        string getFullName();
        void setDisplayName(string dname);
};

Profile::Profile(string uname, string dname) {
    username = uname;
    displayname = dname;
}

Profile::Profile() {
    username = "";
    displayname = "";
}

string Profile::getUsername() {
    // 4
    return username;
}

string Profile::getFullName() {
    // 3
    return displayname + " (@" + username + ")";
}

void Profile::setDisplayName(string dname) {
    // 5
    displayname = dname;
}

class Network {
    // 6
private:
    static const int MAX_USERS=20;
    int numUsers;
    Profile profiles[MAX_USERS];
    int findId(string uname);
    bool following[MAX_USERS][MAX_USERS];
public:
    Network();
    bool addUser(string uname, string dname);
    bool follow(string user_1, string user_2);
    void printDot();
};

Network::Network() {
    numUsers = 0;
    for (int i=0; i < MAX_USERS; i++) {
        for (int j=0; j < MAX_USERS; j++) {
            following[i][j] = false;
        }
    }
}

int Network::findId(string uname) {
    for (int i=0; i < numUsers; i++) {
        if (profiles[i].getUsername() == uname) {
            return i;
        }
    }
    return -1;
}

bool Network::follow(string user_1, string user_2) {
    int row = findId(user_1);
    int col = findId(user_2);

    following[row][col] = true;
    return true;
}

void Network::printDot() {
    cout << "digraph {" << endl;
    for (int i=0; i < numUsers; i++) {
        cout << "  \"@" << profiles[i].getUsername() << "\"" << endl;
    }
    for (int i=0; i < numUsers; i++) {
        for (int j=0; j < numUsers; j++) {
            if (following[i][j]) {
                cout << "  \"@" << profiles[i].getUsername() << "\" -> \"@" << profiles[j].getUsername() << "\"" << endl;
            }
        }
    }
    cout << "}" << endl;
}

bool Network::addUser(string uname, string dname) {
    if (numUsers == 20) {
        return false;
    }
    else if (uname.size() == 0) {
        return false;
    }
    for (int i=0; i < uname.size(); i++) {
        if (! isalnum(uname[i])) {
            return false;
        }
    }
    for (int i=0; i < numUsers; i++) {
        if (profiles[i].getUsername() == uname) {
            return false;
        }
    }
    // comments
    Profile p = Profile(uname, dname);
    profiles[numUsers] = p;
    numUsers++;
    return true;
}

int main() {
    Network nw;
    // add three users
    nw.addUser("mario", "Mario");
    nw.addUser("luigi", "Luigi");
    nw.addUser("yoshi", "Yoshi");

    // make them follow each other
    nw.follow("mario", "luigi");
    nw.follow("mario", "yoshi");
    nw.follow("luigi", "mario");
    nw.follow("luigi", "yoshi");
    nw.follow("yoshi", "mario");
    nw.follow("yoshi", "luigi");

    // add a user who does not follow others
    nw.addUser("wario", "Wario");
    
    // add clone users who follow @mario
    for(int i = 2; i < 6; i++) {
        string usrn = "mario" + to_string(i);
        string dspn = "Mario " + to_string(i);
        nw.addUser(usrn, dspn);
        nw.follow(usrn, "mario");
    }
    // additionally, make @mario2 follow @luigi
    nw.follow("mario2", "luigi");

    nw.printDot();
}

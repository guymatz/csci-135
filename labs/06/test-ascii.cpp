/* 
 * Guy Matz
 * CSCI - 136
 * Ming Nguyen
 * Show ASCII for input
*/

#include <iostream>
#include <cctype>
using namespace std;

// a comment
int main() {
    string text;
    cout << "please enter a text: ";
    getline(cin, text);

    for (int i=0; i < text.length(); i++) {
        cout << text[i] << " " << (int)text[i] << endl;
    }
}

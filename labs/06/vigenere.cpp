/* 
 * Guy Matz
 * CSCI - 136
 * Ming Nguyen
 * functions implementing Caesar cipher encryption
*/

#include <iostream>
#include <cctype>
using namespace std;

// A helper function to shift one character by rshift
char shiftChar(char c, int rshift);

// Caesar cipher encryption
string encryptCaesar(string plaintext, int rshift);

// Vigenere cipher encryption
string encryptVigenere(string plaintext, string keyword);

// a comment
int main() {
    string plaintext;
    string keyword;
    int rshift;
    cout << "Enter plaintext: ";
    getline(cin, plaintext);
    cout << "Enter keyword: ";
    cin >> keyword;

    cout << "Ciphertext: " << encryptVigenere(plaintext, keyword) << endl;;
}

char shiftChar(char c, int rshift) {
    int newc = (int)c;
    if (newc > 64 and newc < 91) {
        newc -= 65;
        newc = (newc + rshift) % 26 + 65;
    }
    else if (newc > 96 and newc < 123) {
        newc -= 97;
        newc = (newc + rshift) % 26 + 97;
    }
    //cout << c << " " << (int)c << " " << (char)newc << " " << newc << endl;
    return char(newc);
}

string encryptVigenere(string plaintext, string keyword) {
    string etext;

    int elem;
    int shift;
    int ctr=0;
    char elem_c, new_char;
    for (int i = 0; i < plaintext.length(); i++) {
        elem = ctr % keyword.length();
        elem_c = keyword[elem];
        shift = (int)elem_c - 'a'; // assume lowercase keyword
        // cout << elem << " " << elem_c << " " << shift << endl;
        new_char = shiftChar(plaintext[i], shift);
        if (isalpha(plaintext[i])) {
            ctr++;
        }
        etext = etext + new_char;
    }

    return etext;
}

/* 
 * Guy Matz
 * CSCI - 136
 * Ming Nguyen
 * functions implementing Caesar cipher encryption
*/

#include <iostream>
#include <cctype>
using namespace std;

// A helper function to shift one character by rshift
char shiftChar(char c, int rshift);

// Caesar cipher encryption
string encryptCaesar(string plaintext, int rshift);

// a comment
int main() {
    string plaintext;
    int rshift;

    cout << "Enter plaintext: ";
    getline(cin, plaintext);
    cout << "Enter shift: ";
    cin >> rshift;

    cout << encryptCaesar(plaintext, rshift) << endl;;
}

char shiftChar(char c, int rshift) {
    char newc;
    if (isupper(c)) {
        newc = (c - 'A' + rshift) % 26 + 'A';
    }
    else if (islower(c)) {
        newc = (c - 'a' + rshift) % 26 + 'a';
    }
    else {
        newc = c;
    }
    //cout << c << " " << (int)c << " " << (char)newc << " " << newc << endl;
    return newc;
}

string encryptCaesar(string plaintext, int rshift) {
    string etext = "";

    for (int i = 0; i < plaintext.length(); i++) {
        etext = etext + shiftChar(plaintext[i], rshift);
    }

    return etext;
}

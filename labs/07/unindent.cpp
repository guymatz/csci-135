/* 
 * Guy Matz
 * CSCI - 136
 * Ming Nguyen
 * Show ASCII for input
*/

#include <iostream>
#include <cctype>
using namespace std;

string removeLeadingSpaces(string line) {

    string new_str = "";
    bool started = false;
    for (int i=0; i < line.length(); i++) {
        if (! isspace(line[i])) {
            started = true;
        }
        if (started) {
            new_str += line[i];
        }
    }
    return new_str;
}


// a comment
int main() {

    string text;
    while (getline(cin, text)) {
        cout << removeLeadingSpaces(text) << endl;
    }

    //cout << removeLeadingSpaces("     int x = 1;   ");

    return 0;
}


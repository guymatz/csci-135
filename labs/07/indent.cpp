/* 
 * Guy Matz
 * CSCI - 136
 * Ming Nguyen
 * Show ASCII for input
*/

#include <iostream>
#include <cctype>
using namespace std;

int countChar(string line, char c) {
    int ctr = 0;
    for (int i=0; i < line.length(); i++) {
        if (line[i] == c) ctr++;
    }
    return ctr;
}

string removeLeadingSpaces(string line) {

    string new_str = "";
    bool started = false;
    for (int i=0; i < line.length(); i++) {
        if (! isspace(line[i])) {
            started = true;
        }
        if (started) {
            new_str += line[i];
        }
    }
    return new_str;
}


// a comment
int main() {

    string indented_line;
    // a comment
    string text;
    int opening_blocks=0;
    int opening_braces=0;
    int closing_braces=0;
    int indentation_level=0;
    while (getline(cin, text)) {
        indented_line = removeLeadingSpaces(text);
        indentation_level = opening_blocks;
        // remove padding
        if (indented_line[0] == '}') {
            indentation_level = opening_blocks - 1;
        }
        for (int j=0; j < indentation_level; j++) {
            cout << "\t";
        }
        opening_braces = countChar(indented_line, '{');
        // add padding
        closing_braces = countChar(indented_line, '}');
        opening_blocks += (opening_braces - closing_braces);
        cout << indented_line << endl;
    }

    //cout << removeLeadingSpaces("     int x = 1;   ");

    return 0;
}


/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 1, month length

28, 30 or 31 days?
*/

#include <iostream>
using namespace std;

int main() {
    int year, month, days;
    cout << "Enter year: ";
    cin >> year;
    cout << "Enter month: ";
    cin >> month;

    // check leap year vs common year in feb first
    if (month == 2) {
        // another comment
        if ( year % 4 != 0) {
            days = 28;
        }
        else if ( year % 100 != 0) {
            days = 29;
        }
        else if ( year % 400 != 0) {
            days = 28;
        }
        else {
            days = 29;
        }
    }
    else if ( month == 4 || month == 6 || month == 9 || month == 11) {
       days = 30;
    }
    else {
        days = 31;
    }

    cout << days << " days";

    return 0;
}

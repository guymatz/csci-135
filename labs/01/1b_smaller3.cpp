/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 1, smaller

Smaller of 3 numbers
*/

#include <iostream>
using namespace std;

int main() {
    int first, second, third, smallest;
    cout << "Enter the first number: ";
    cin >> first;
    cout << "Enter the second number: ";
    cin >> second;
    cout << "Enter the third number: ";
    cin >> third;

    cout << "The smaller of the three is ";
    // another comment
    if (first < second && first < third) {
        smallest = first;
    }
    else if (second < first && second < third) {
        smallest = second;
    }
    else {
        smallest = third;
    }
    cout << smallest;

    return 0;
}

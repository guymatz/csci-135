//  Here's a comment
/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 5,
*/

#include <iostream>
using namespace std;

bool isDivisibleBy(int one, int two) {
    if ( two == 0 ) {
        return false;
    }
    else {
        return one % two == 0;
    }
}

bool isPrime(int n) {
    // here's a comment
    if (n <= 1) {
        return false;
    }
    else if (n == 2) {
        return true;
    }
    if (n % 2 == 0) {
        return false;
    }
    for (int i=3; i < n/2; i=i+2) {
        if (isDivisibleBy(n, i)) {
            return false;
        }
    } 
    return true;
}

int nextPrime(int n) {
    //cout << "Passed in " << n << endl;

    int i = n+1;
    do {
        //cout << "checking if " << i << " is prime" << endl;
        if (isPrime(i)) {
            return i;
        }
        i++;
    } while (1);
}

int countPrimes(int a, int b) {
    int num_primes = 0;
    for (int i=a; i <= b; i++) {
        if (isPrime(i)) {
            num_primes++;
        }
    }
    return num_primes;
}

bool isTwinPrime(int n) {
    return isPrime(n) and (isPrime(n-2) or isPrime(n+2));
}

int nextTwinPrime(int n) {
    int i=n+1;
    do {
        //cout << "checking if " << i << " is prime" << endl;
        if (isTwinPrime(i)) {
            return i;
        }
        i++;
    } while (1);
}

int largestTwinPrime(int a, int b) {
    int largest = -1;
    for (int i=b; i >=a; i--) {
        if (isTwinPrime(i)) {
            return i;
        }
    }
    return largest;
}

int main() {

    // comments
    int num1, num2;
    cout << "Enter a number: ";
    cin >> num1;
    cout << "Enter another number: ";
    cin >> num2;
    /*
    */

    /*
    if (isPrime(num1)) {
        cout << "true" << endl;
    }
    else {
        cout << "false" << endl;
    }
    */

    //cout << nextPrime(num1);
    //cout << countPrimes(num1, num2);
    //cout << isTwinPrime(num1);
    //cout << nextTwinPrime(num1);
    cout << largestTwinPrime(num1, num2);
}

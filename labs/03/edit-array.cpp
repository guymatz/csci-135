/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 2, 
Write a program edit-array.cpp that creates an array of 10 integers, and
provides the user with an interface to edit any of its elements.
Specifically, it should work as follows:

    1. Create an array myData of 10 integers.
    2. Fill all its cells with value 1 (using a for loop).
    3. Print all elements of the array on the screen.
    4. Ask the user to input the cell index i, and its new value v.
    5. If the index i is within the array range (0 ≤ i < 10),
       update the asked cell, myData[i] = v, and go back to the step 3.
       Otherwise, if index i is out of range, the program exits.


*/

#include <iostream>
#include <cmath>
using namespace std;

int main() {
    int list[10];
    int input_value = -1;
    int input_index = -1;

    for (int i = 0; i < 10; i++) {
        list[i] = 1;
    }

    do {
        for (int i = 0; i < 10; i++) {
            cout << list[i] << " ";
        }
        cout << endl;

        cout << "Input index: ";
        cin >> input_index;
        cout << "Input value: ";
        cin >> input_value;

        if (input_index >= 0 && input_index < 10) {
            list[input_index] = input_value;
        }
    } while (input_index >= 0 && input_index < 10);

    return 0;
}

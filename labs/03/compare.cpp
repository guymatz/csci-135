/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3, 
  Write a program minmax.cpp that finds the minimum and maximum storage in East basin in 2018.

*/

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <climits>
#include <string>
using namespace std;

int main() {
    
    string filename = "Current_Reservoir_Levels.tsv";
    ifstream fin(filename);
    if (fin.fail()) {
        cerr << "File, " << filename << ", cannot be opened.";
        exit(1);
    }

    // get rid of junk
    string junk;
    getline(fin, junk);
    // cout << "Junk: " << junk;

    string start_date, end_date, date;
    cout << "Enter starting date: ";
    cin >> start_date;
    cout << "Enter ending date: ";
    cin >> end_date;
    double evol, elog, wvol, wlog;
    while (fin >> date >> evol >> elog >> wvol >> wlog) {
        // span the dates
        if (date >= start_date and date <= end_date) {
            if (elog < wlog) {
                cout << date << " West" << endl;
            }
            else if (wlog < elog) {
                cout << date << " East" << endl;
            }
            else {
                cout << date << " Equal" << endl;
            }
            
        }
    }

    return 0;
}

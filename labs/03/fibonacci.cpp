/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 2, 
Write a program fibonacci.cpp, which uses an array of ints to compute and
print all Fibonacci numbers from F(0) to F(59).

*/

#include <iostream>
#include <cmath>
using namespace std;

int main() {
    int list[60];

    list[0] = 0;
    list[1] = 1;
    for (int i = 2; i < 60; i++) {
        list[i] = list[i-2] + list[i-1];
    }

    // when the sequence gets abbove 2.4 bill we have an issue with ints
    for (int i = 0; i < 60; i++) {
        cout << list[i] << endl;
    }

    return 0;
}

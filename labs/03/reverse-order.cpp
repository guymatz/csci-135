/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3, 
  Write a program minmax.cpp that finds the minimum and maximum storage in East basin in 2018.

*/

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <climits>
#include <string>
using namespace std;

int main() {
    
    string filename = "Current_Reservoir_Levels.tsv";
    ifstream fin(filename);
    if (fin.fail()) {
        cerr << "File, " << filename << ", cannot be opened.";
        exit(1);
    }

    // get rid of junk
    string junk;
    getline(fin, junk);
    // cout << "Junk: " << junk;

    int index=0;
    // sould really only need 365
    double west_elev_arr[400];
    string date_arr[400];
    string earlier_date, later_date, date;
    cout << "Enter earlier date: ";
    cin >> earlier_date;
    cout << "Enter later date: ";
    cin >> later_date;
    double evol, elog, wvol, wlog;
    while (fin >> date >> evol >> elog >> wvol >> wlog) {
        // increment index so we can llop later
        // span the dates
        if (date >= earlier_date and date <= later_date) {
            // cout << date << " " << wlog << " " << index << endl;
            west_elev_arr[index] = wlog;
            date_arr[index] = date;
            index++;;
        }
    }

    for (int i = index; i >= 0; i--) {
        cout << date_arr[i] << " " << west_elev_arr[i] << endl;
    }
    return 0;
}

/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 2, 
Write a program print-interval.cpp that asks the user to input two
integers L and U (representing the lower and upper limits of the interval),
and print out all integers in the range L ≤ i < U separated by spaces.
Notice that we include the lower limit, but exclude the upper limit

*/

#include <iostream>
#include <cmath>
using namespace std;

int main() {
    int lower, upper = -1;

    cout << "Please enter L: ";
    cin >> lower;
    cout << "Please enter U: ";
    cin >> upper;
    for (int i = lower ; i < upper; i++) {
        cout << i << " ";
    }

    return 0;
}

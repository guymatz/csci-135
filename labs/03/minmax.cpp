/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3, 
  Write a program minmax.cpp that finds the minimum and maximum storage in East basin in 2018.

*/

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <climits>
#include <string>
#include <cfloat>
using namespace std;

int main() {
    
    string filename = "Current_Reservoir_Levels.tsv";
    ifstream fin(filename);
    if (fin.fail()) {
        cerr << "File, " << filename << ", cannot be opened.";
        exit(1);
    }

    string junk;
    getline(fin, junk);
    // cout << "Junk: " << junk;

    // skipping header
    string date;
    double evol, elog, wvol, wlog;
    double min=DBL_MAX, max=-DBL_MAX;
    while (fin >> date >> evol >> elog >> wvol >> wlog) {
        if (evol > max) {
            max = evol;
        }
        else if (evol < min) {
            min = evol;
        }
    }

    cout << "minimum storage in East basin: " << min << " billion gallons" << endl;
    cout << "MAXimum storage in East basin: " << max << " billion gallons" << endl;
    return 0;
}

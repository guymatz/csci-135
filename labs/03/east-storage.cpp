/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3, 
  Write a program east-storage.cpp that asks the user to input a string
  representing the date (in MM/DD/YYYY format), and prints out the East
  basin storage on that day

*/

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <climits>
#include <string>
using namespace std;

int main() {
    string date_in; 

    cout << "Enter date: ";
    cin >> date_in;
    
    string filename = "Current_Reservoir_Levels.tsv";
    ifstream fin(filename);
    if (fin.fail()) {
        cerr << "File, " << filename << ", cannot be opened.";
        exit(1);
    }

    // skipping headers
    string junk;
    getline(fin, junk);
    // cout << "Junk: " << junk;

    string date, evol, elog, wvol, wlog;
    double that_thing;
    while (fin >> date >> evol >> elog >> wvol >> wlog) {
        if (date_in == date) {
            cout << "East base storage: " << evol << " billion gallons" << endl;
        }
    }

    return 0;
}

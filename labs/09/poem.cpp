// a comment
#include <iostream>
using namespace std;

// a comment
string* createAPoemDynamically() {
    // a comment
    string *p = new string;
    *p = "Roses are red, violets are blue";
    // a comment
    return p;
}

// a comment
int main() {
    while(true) {
        string *p;
        // a comment
        p = createAPoemDynamically();

        // assume that the poem p is not needed at this point
        delete p;

    }
}

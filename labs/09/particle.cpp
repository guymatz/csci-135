/*
  Author: Guy Matz

  Description:
    declare a new class Particle, which 
    stores position and velocity of the particle
*/
#include <iostream>
#include <cmath>
using namespace std;

class Coord3D {
    public:
        double x, y, z;
};

class Particle {
    public:
        double x, y, z;
        double vx, vy, vz;
};

double length(Coord3D *p);
Coord3D * fartherFromOrigin(Coord3D *p1, Coord3D *p2);
void move(Coord3D *ppos, Coord3D *pvel, double dt);
Coord3D* createCoord3D(double x, double y, double z);
void deleteCoord3D(Coord3D *p);
// new particle methods
Particle* createParticle(double x, double y, double z, 
                         double vx, double vy, double vz);
void setVelocity(Particle *p, double vx, double vy, double vz);
Coord3D getPosition(Particle *p);
void move(Particle *p, double dt);
void deleteParticle(Particle *p);


int main() {

    /*
    Coord3D pointA = {10, 20, 30};
    cout << length(&pointA) << endl; // would print 37.4166

    Coord3D pointP = {10, 20, 30};
    Coord3D pointQ = {-20, 21, -22};

    cout << "Address of P: " << &pointP << endl;
    cout << "Address of Q: " << &pointQ << endl << endl;

    Coord3D * ans = fartherFromOrigin(&pointP, &pointQ);
   
    cout << "ans = " << ans << endl; // So which point is farther?
    */

    /*
    Coord3D pos = {0, 0, 100.0};
    Coord3D vel = {1, -5, 0.2};

    move(&pos, &vel, 2.0); // object pos gets changed
    cout << pos.x << " " << pos.y << " " << pos.z << endl;
    // prints: 2 -10 100.4
    */

    /*
    double x, y, z;
    cout << "Enter position: ";
    cin >> x >> y >> z;
    Coord3D *ppos = createCoord3D(x,y,z);
    
    cout << "Enter velocity: ";
    cin >> x >> y >> z;
    Coord3D *pvel = createCoord3D(x,y,z);

    move(ppos, pvel, 10.0);

    cout << "Coordinates after 10 seconds: " 
         << (*ppos).x << " " << (*ppos).y << " " << (*ppos).z << endl;

    deleteCoord3D(ppos); // release memory
    deleteCoord3D(pvel);
    */

    Particle *p = createParticle(1.0, 1.5, 2.0, 0.1, 0.2, 0.3);
    double time = 0.0;
    double dt = 0.1;
    while(time < 3.0) {
        // update its velocity
        setVelocity(p, 10.0 * time, 0.3, 0.1);

        // move the particle
        move(p, dt);
        time += dt;

        // reporting its coordinates
        cout << "Time: " << time << " \t";
        cout << "Position: "
             << getPosition(p).x << ", "
             << getPosition(p).y << ", "
             << getPosition(p).z << endl;
    }
    // remove the particle, deallocating its memory
    deleteParticle(p);
    
    return 0;
}

Particle* createParticle(double x, double y, double z, double vx, double vy, double vz) {
    Particle *p = new Particle;
    p->x = x;
    p->y = y;
    p->z = z;
    p->vx = vx;
    p->vy = vy;
    p->vz = vz;
    return p;
}

void setVelocity(Particle *p, double vx, double vy, double vz) {
    p->vx = vx;
    p->vy = vy;
    p->vz = vz;
}

Coord3D getPosition(Particle *p) {
    Coord3D *c = new Coord3D;
    c->x = p->x;
    c->y = p->y;
    c->z = p->z;
    return *c;
}

void move(Particle *p, double dt) {
    p->x += p->vx * dt;
    p->y += p->vy * dt;
    p->z += p->vz * dt;
}

void deleteParticle(Particle *p) {
    delete p;
}

Coord3D* createCoord3D(double x, double y, double z) {
    Coord3D *p = new Coord3D;
    p->x = x;
    p->y = y;
    p->z = z;
    return p;
}

void deleteCoord3D(Coord3D *p) {
    delete p;
}

void move(Coord3D *ppos, Coord3D *pvel, double dt) {
    ppos->x += pvel->x * dt;
    ppos->y += pvel->y * dt;
    ppos->z += pvel->z * dt;
}

double length(Coord3D *p) {
    return sqrt( pow(p->x, 2) + pow(p->y, 2) + pow(p->z, 2) );
}

Coord3D * fartherFromOrigin(Coord3D *p1, Coord3D *p2) {
    double dist1 = length(p1);
    double dist2 = length(p2);

    if (dist1 > dist2) {
        return p1;
    }
    else {
        return p2;
    }
}

/*
  Author:   <---  Write your name here

  Description:
    The program reads a PGM image from the file "inImage.pgm",
    and outputs a modified image to "outImage.pgm"
*/


#include <iostream>
#include <cassert>
#include <cstdlib>
#include <fstream>

using namespace std;

const int MAX_H = 512;
const int MAX_W = 512;

// Reads a PGM file.
// Notice that: height and width are passed by reference!
void readImage(int image[MAX_H][MAX_W], int &height, int &width) {
	char c;
	int x;
	ifstream instr;
	instr.open("inImage.pgm");

	// read the header P2
	instr >> c;
	assert(c == 'P');
	instr >> c;
	assert(c == '2');

	// skip the comments (if any)
	while ((instr>>ws).peek() == '#') {
		instr.ignore(4096, '\n');
	}

	instr >> width;
	instr >> height;

	assert(width <= MAX_W);
	assert(height <= MAX_H);
	int max;
	instr >> max;
	assert(max == 255);

	for (int row = 0; row < height; row++)
		for (int col = 0; col < width; col++)
			instr >> image[row][col];
	instr.close();
	return;
}


// Writes a PGM file
// Need to provide the array data and the image dimensions
void writeImage(int image[MAX_H][MAX_W], int height, int width) {
	ofstream ostr;
	ostr.open("outImage.pgm");
	if (ostr.fail()) {
		cout << "Unable to write file\n";
		exit(1);
	};

	// print the header
	ostr << "P2" << endl;
	// width, height
	ostr << width << ' ';
	ostr << height << endl;
	ostr << 255 << endl;

	for (int row = 0; row < height; row++) {
		for (int col = 0; col < width; col++) {
			assert(image[row][col] < 256);
			assert(image[row][col] >= 0);
			ostr << image[row][col] << ' ';
			ostr << endl;
		}
	}
	ostr.close();
	return;
}


int main() {

	int img[MAX_H][MAX_W];
	int H, W;

	readImage(img, H, W); // read it from the file "inImage.pgm"
	// h and w were passed by reference and
	// now contain the dimensions of the picture
	// and the 2-dimesional array img contains the image data

	// Now we can manipulate the image the way we like
	// for example we copy its contents into a new array
	int out[MAX_H][MAX_W];
    int func;
    int a,b,c,d,e,f,g,h,i;
	for(int row = 0; row < H; row=row+1) {
		for(int col = 0; col < W; col=col+1) {
            if (row == 0 or col == 0 or row == MAX_H or col == MAX_W) {
                out[row][col] = img[row][col];
                cout << row << " " << col << " = " << img[row][col] << endl;
            }
            else {
        	    a = img[row-1][col-1];
        	    b = img[row-1][col];
        	    c = img[row-1][col+1];
                /*
        	    d = img[row][col-1];
        	    e = img[row][col];
        	    f = img[row][col+1];
                */
        	    g = img[row+1][col-1];
        	    h = img[row+1][col];
        	    i = img[row+1][col+1];
    
        	    func = (g+2*h+i) - (a+2*b+c);
                if (func > 255) {
                    func = 255;
                }
                else if (func < 0) {
                    func = 0;
                }
        	    out[row][col] = func;
                cout << row << " " << col << " = " << func << endl;
            }
		}
	}

	// and save this new image to file "outImage.pgm"
	writeImage(out, H, W);

}

/*
  Author:   <---  Write your name here

  Description:
    The program reads a PGM image from the file "inImage.pgm",
    and outputs a modified image to "outImage.pgm"
*/


#include <iostream>

using namespace std;

class Time {
    public:
        int h;
        int m;
};

enum Genre {ACTION, COMEDY, DRAMA, ROMANCE, THRILLER};

class Movie {
    public:
        string title;
        Genre genre;
        int duration; // in minutes
};

class TimeSlot {
    public:
        Movie movie;
        Time startTime;
};

void printTime(Time time) {
    cout << time.h << ":" << time.m;
}

int minutesSinceMidnight(Time time);
int minutesUntil(Time earlier, Time later);
Time addMinutes(Time time0, int min);
void printMovie(Movie mv);
void printTimeSlot(TimeSlot ts);
TimeSlot scheduleAfter(TimeSlot ts, Movie nextMovie);
bool timeOverlap(TimeSlot ts1, TimeSlot ts2);

int main() {
    
    Movie movie1 = {"Back to the Future", COMEDY, 116};
    Movie movie2 = {"Black Panther", ACTION, 134};
    
    Movie movie3 = {"The Wolf of Wall Street", COMEDY, 180};
    Movie movie4 = {"5 Centimeters Per Second", DRAMA, 53};
    
    TimeSlot morning = {movie1, {9, 15}};  
    TimeSlot daytime = {movie2, {15, 15}}; 
    TimeSlot evening = {movie2, {14, 45}}; 
    TimeSlot wolf = {movie3, {10, 30}}; 
    TimeSlot five = {movie4, {11, 30}}; 

    if (timeOverlap(morning, daytime)) {
        cout << "overlap" << endl;
    }
    else {
        cout << "no" << endl;
    }

    if (timeOverlap(daytime, evening)) {
        cout << "overlap" << endl;
    }
    else {
        cout << "no" << endl;
    }

    if (timeOverlap(wolf, five)) {
        cout << "overlap" << endl;
    }
    else {
        cout << "no" << endl;
    }

    return 0;
}

bool timeOverlap(TimeSlot ts1, TimeSlot ts2) {
    printTimeSlot(ts1);
    printTimeSlot(ts2);
    int ts1Start = minutesSinceMidnight(ts1.startTime);
    int ts1End = minutesSinceMidnight(addMinutes(ts1.startTime, ts1.movie.duration));
    int ts2Start = minutesSinceMidnight(ts2.startTime);
    int ts2End = minutesSinceMidnight(addMinutes(ts2.startTime, ts2.movie.duration));
    cout << ts1Start << " " << ts1End << " " << ts2Start << " " << ts2End << " " << endl;
    if ( (ts1Start >= ts2Start and ts1Start <= ts2End) or (ts1End >= ts2Start and ts1End <= ts2End) ) {
        return true;
    }
    else if ( (ts2Start >= ts1Start and ts2Start <= ts1End) or (ts2End >= ts1Start and ts2End <= ts1End) ) {
        return true;
    }
    else {
        return false;
    }
}

TimeSlot scheduleAfter(TimeSlot ts, Movie nextMovie) {
    /* 
     produce and return a new TimeSlot for the movie nextMovie,
     scheduled immediately after the time slot ts.

     For example, if the movie scheduled in ts starts at 14:10
     and lasts 120 minutes, then the time slot for the next movie
     should start at exactly 16:10
     */

    Time start = ts.startTime;
    Time nextStart = addMinutes(start, ts.movie.duration);
    TimeSlot nextTs = {nextMovie, nextStart};

    return nextTs;
}

void printTimeSlot(TimeSlot ts) {
    // Does C++ allow me to define a str() function for a class?
    Time start = ts.startTime;
    Time end = addMinutes(ts.startTime, ts.movie.duration);
    printMovie(ts.movie);
    cout << " [starts at ";
    // Then I wouldn't have to do this:
    cout << start.h << ":" << start.m;
    cout << ", ends by ";
    // Or this
    cout << end.h << ":" << end.m;
    cout << "]" << endl;
}

void printMovie(Movie mv) {
    string g;
    switch (mv.genre) {
        case ACTION   : g = "ACTION"; break;
        case COMEDY   : g = "COMEDY"; break;
        case DRAMA    : g = "DRAMA";  break;
        case ROMANCE  : g = "ROMANCE"; break;
        case THRILLER : g = "THRILLER"; break;
    }
    cout << mv.title << " " << g << " (" << mv.duration << " min)";
}
// return a new moment of time that is min minutes after time0
Time addMinutes(Time time0, int min) {
    int t_hour = time0.h;
    int t_min = time0.m;
    int new_h = 0;
    int new_m = 0;

    // here's a comment
    new_m =  (t_min+min) % 60;
    new_h = time0.h + ((t_min + min) / 60);

    Time t = {new_h, new_m};
    return t;
}

// return the number of minutes from 0:00AM until time
int minutesSinceMidnight(Time time) {
    // here's a comment
    return time.h * 60 + time.m;
}

// how many minutes separate the two moments
int minutesUntil(Time earlier, Time later) {
    int e_hours = earlier.h;
    int e_mins = earlier.m;
    int l_hours = later.h;
    int l_mins = later.m;

    int mins = 0;

    if (l_mins < e_mins) {
        l_hours--;
        mins = 60 - (e_mins - l_mins);
    }
    else {
        mins = l_mins - e_mins;
    }

    mins += 60*(l_hours - e_hours);

    return mins;
}

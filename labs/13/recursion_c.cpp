/*
  Author:   <---  Write your name here

  Description:
    implement the class Profile that can store the info about
    a user of the network.

*/


#include <iostream>

using namespace std;

void printRange(int left, int right);
int sumRange(int left, int right);
int sumArray(int *arr, int size);

int main() {
    int size = 10;
    int *arr = new int[size]; // allocate array dynamically
    arr[0] = 12;
    arr[1] = 17;
    arr[2] = -5;
    arr[3] = 3;
    arr[4] = 7;
    arr[5] = -15;
    arr[6] = 27;
    arr[7] = 5;
    arr[8] = 13;
    arr[9] = -21;
    //cout << arr[size-1] << endl;
    cout << sumArray(arr, size) << endl;
    cout << sumArray(arr, 5) << endl;

    delete[] arr;
    return 0;
}

int sumArray(int *arr, int size) {
    // comment
    // cout << size << " " << arr[size-1] << endl;;
    if (size == 0) {
        return 0;
    }
    else if (size == 1) {
        //cout << "Adding " << arr[1] << " & " <<  arr[0] << endl;
        return arr[0];
    }
    else {
        return arr[size-1] + sumArray(arr, size-1);
    }
}

int sumRange(int left, int right) {
    // comment
    if (left > right) {
        return 0;
    }
    //cout << left << " ";
    return left + sumRange(left+1, right);
}

void printRange(int left, int right) {
    // comment
    if (left > right) {
        return;
    }
    cout << left << " ";
    left++;
    printRange(left, right);
}

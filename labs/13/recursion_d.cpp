/*
  Author:   <---  Write your name here

  Description:
    implement the class Profile that can store the info about
    a user of the network.

*/


#include <iostream>
#include <cctype>
#include <string>

using namespace std;

void printRange(int left, int right);
int sumRange(int left, int right);
int sumArray(int *arr, int size);
bool isAlphanumeric(string s);

int main() {
    
    cout << isAlphanumeric("ABCD") << endl;        // true (1)
    cout << isAlphanumeric("Abcd1234xyz") << endl; // true (1)
    cout << isAlphanumeric("KLMN 8-7-6") << endl;  // false (0)
    return 0;
}

bool isAlphanumeric(string s) {
    if (s.size() == 0) {
        return true;
    }
    int last = s.size() - 1;
    //cout << s[last] << " " << isalnum(s[last]) << endl;
    return isalnum(s[last]) and isAlphanumeric(s.substr(0, last));
}

int sumArray(int *arr, int size) {
    // comment
    // cout << size << " " << arr[size-1] << endl;;
    if (size == 0) {
        return 0;
    }
    else if (size == 1) {
        //cout << "Adding " << arr[1] << " & " <<  arr[0] << endl;
        return arr[0];
    }
    else {
        return arr[size-1] + sumArray(arr, size-1);
    }
}

int sumRange(int left, int right) {
    // comment
    if (left > right) {
        return 0;
    }
    //cout << left << " ";
    return left + sumRange(left+1, right);
}

void printRange(int left, int right) {
    // comment
    if (left > right) {
        return;
    }
    cout << left << " ";
    left++;
    printRange(left, right);
}

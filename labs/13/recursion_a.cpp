/*
  Author:   <---  Write your name here

  Description:
    implement the class Profile that can store the info about
    a user of the network.

*/


#include <iostream>

using namespace std;

void printRange(int left, int right);

int main() {
    printRange(-2, 10);
}

void printRange(int left, int right) {
    // comment
    if (left > right) {
        return;
    }
    cout << left << " ";
    left++;
    printRange(left, right);
}

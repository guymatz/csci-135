/*
  Author:   <---  Write your name here

  Description:
    implement the class Profile that can store the info about
    a user of the network.

*/


#include <iostream>
#include <cctype>
#include <string>

using namespace std;

void printRange(int left, int right);
int sumRange(int left, int right);
int sumArray(int *arr, int size);
bool isAlphanumeric(string s);
bool nestedParens(string s);
bool divisible(int *prizes, int size);
bool distribute(int *prices, int size, int alice, int bob);

int main() {
    
    //int prices [] = {10, 15, 12, 18, 19, 17, 13, 35, 33};
    int prices [] = {18, 5, 15, 10, 12};
    cout << divisible(prices, 5);
    return 0;
}

bool distribute(int *prices, int size, int alice, int bob){

    cout << "Common: " << alice << " " << bob << endl;

    if(size == 0){ 
        cout << "Does alice == bob? " << (alice == bob) << endl;
        return (alice == bob);
    }
    if (distribute(prices, size-1, alice+prices[size-1], bob)){
        cout << "Alice: " << alice << " " << bob << endl;
        return true;
    } 

    if (distribute(prices, size-1, alice, bob+prices[size-1])){ 
        cout << "Bob: " << alice << " " << bob << endl;
        return true;
    } 

    return false;
}
bool divisible(int *prices, int size) {
    return distribute(prices, size, 0, 0);
    /*
    int sum = sumum(prices, size);
    if (sum % 2 != 0) {
        return false;
    }
    int half = sum / 2;
    int total = 0;
    for (int i=0; i < size; i++) {
        total = prices[i];
        for (int j = i+1; j < size; j++) {
            cout << "Total: " << total << " : " << prices[j] << endl;
            if (total + prices[j] == half) {
                cout << "Yay!" << endl;
                return true;
            }
            else if (total + prices[j] > half) {
                cout << "skipping " << prices[j] << endl;
                continue;
            }
            else {
                total += prices[j];
            }
        }
    }
    return false;
    */
}


int sumum(int *prices, int size) {
    int total = 0;
    for (int i=0; i < size; i++) {
        total += prices[i];
    }
    return total;
}


bool nestedParens(string s) {
    if (s.size() == 0) {
        return true;
    }
    if ((s.size() % 2 != 0) or !(s[0]=='(' and s[s.size()-1]==')')) {
        //cout << s << ": " << s.size() << endl;
        return false;
    }
    else {
        return nestedParens(s.substr(1,s.size()-2));
    }
}

bool isAlphanumeric(string s) {
    if (s.size() == 0) {
        return true;
    }
    int last = s.size() - 1;
    //cout << s[last] << " " << isalnum(s[last]) << endl;
    return isalnum(s[last]) and isAlphanumeric(s.substr(0, last));
}

int sumArray(int *arr, int size) {
    // comment
    // cout << size << " " << arr[size-1] << endl;;
    if (size == 0) {
        return 0;
    }
    else if (size == 1) {
        //cout << "Adding " << arr[1] << " & " <<  arr[0] << endl;
        return arr[0];
    }
    else {
        return arr[size-1] + sumArray(arr, size-1);
    }
}

int sumRange(int left, int right) {
    // comment
    if (left > right) {
        return 0;
    }
    //cout << left << " ";
    return left + sumRange(left+1, right);
}

void printRange(int left, int right) {
    // comment
    if (left > right) {
        return;
    }
    cout << left << " ";
    left++;
    printRange(left, right);
}

/*
  Author:   <---  Write your name here

  Description:
    implement the class Profile that can store the info about
    a user of the network.

*/


#include <iostream>

using namespace std;

void printRange(int left, int right);
int sumRange(int left, int right);

int main() {
    cout << sumRange(-2, 10);
}

int sumRange(int left, int right) {
    // comment
    if (left > right) {
        return 0;
    }
    //cout << left << " ";
    return left + sumRange(left+1, right);
}

void printRange(int left, int right) {
    // comment
    if (left > right) {
        return;
    }
    cout << left << " ";
    left++;
    printRange(left, right);
}

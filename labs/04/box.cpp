/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3,
  Write a program minmax.cpp that finds the minimum and maximum storage in East basin in 2018.

*/

#include <iostream>
using namespace std;

/* this is a comment */
int main() {
    int width, height;

    /* this is a comment */
    cout << "Input width: ";
    cin >> width;
    cout << endl;
    cout << "Input height: ";
    cin >> height;
    cout << endl;

    for (int w=0; w < height; w++) {
        for (int h=0; h < width; h++) {
            cout << "*";
        }
        cout << endl;
    }

    return 0;
}

/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3,
  Write a program minmax.cpp that finds the minimum and maximum storage in East basin in 2018.

*/

#include <iostream>
using namespace std;

/* this is a comment */
int main() {
    int width, height;

    /* this is a comment */
    cout << "Input width: ";
    cin >> width;
    cout << "Input height: ";
    cin >> height;

    if (width - height < width/2.0) {
        cout << "Impossible shape!" << endl;
        exit(1);
    }

    for (int h=0; h < height; h++) {
        for (int s1=0; s1 < h; s1++) {
            cout << " ";
        }
        for (int w=0; w < width - (2 * h); w++) {
            cout << "*";
        }
        for (int s2=0; s2 < h; s2++) {
            cout << " ";
        }
        cout << endl;
    }

    return 0;
}

/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3,
  Write a program minmax.cpp that finds the minimum and maximum storage in East basin in 2018.

*/

#include <iostream>
using namespace std;

int main() {

    int width, height;
    cout << "Input width: ";
    cin >> width;
    cout << "Input height: ";
    cin >> height;

    cout << "Shape:" << endl;

    for (int h=0; h < height; h++) {
        for (int w = 0; w < width; w++) {
            // skip
            if ((h + w) % 2 == 0) {
                cout << "*";
            }
            else {
                cout << " ";
            }
        }
        cout << endl;
    }

    return 0;
}

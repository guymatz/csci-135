/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3,
  Write a program minmax.cpp that finds the minimum and maximum storage in East basin in 2018.

*/

#include <iostream>
using namespace std;

int main() {

    int size;
    cout << "Input size: ";
    cin >> size;

    cout << "Shape:" << endl;

    for (int r=0; r < size; r++) {
        for (int c = 0; c < size; c++) {
            // skip
            if (c == r or c == size-r-1) {
                cout << "*";
            }
            else {
                cout << " ";
            }
        }
        cout << endl;
    }

    return 0;
}

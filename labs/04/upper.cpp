/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3,
  Write a program minmax.cpp that finds the minimum and maximum storage in East basin in 2018.

*/
#include <iostream>
using namespace std;

/* this is a comment */
int main() {
    int side;

    cout << "Input side length: ";
    cin >> side;

    cout << endl;
    /* this is a comment */
    for (int s=0; s < side; s++) {
        for (int s1=0; s1 < s; s1++) {
            cout << " ";
        }
        for (int s2=0; s2 < side - s; s2++) {
            cout << "*";
        }
        cout << endl;
    }

    return 0;
}

/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: Lab 3,
  Write a program minmax.cpp that finds the minimum and maximum storage in East basin in 2018.

*/

#include <iostream>
using namespace std;

int main() {

    int width, height;
    cout << "Input width: ";
    cin >> width;
    cout << "Input height: ";
    cin >> height;

    cout << "Shape:" << endl;

    for (int h=0; h < height; h++) {
        // 3 rows
        for (int j=0; j < 3 and j+h*3 < height; j++) {
            // 3 columns
            for (int w = 0; w*3 < width; w++) {
                for (int i=0; i < 3 and i+w*3 < width; i++) {
                    if ((h + w) % 2 == 0) {
                        cout << "*";
                    }
                    else {
                        cout << " ";
                    }
                }
            }
        cout << endl;
        }
    }

    return 0;
}

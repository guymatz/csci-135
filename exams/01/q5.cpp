#include <iostream>

using namespace std;

int main() {
    int num = 10;

    while (num > 0 and num < 100) {
            cout << "Please enter a number <= 0 or >= 100: ";
            cin >> num;
    }
    cout << num << endl;
}

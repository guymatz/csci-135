#include <iostream>

using namespace std;

int main() {
    string text;
    int word_ctr = 0;
    bool within_word = false;
    getline(cin, text);

    cout << text[text.length()-1] << endl;
    for (int i=0; i < text.length(); i++) {
        if (text[i] == ' ') {
            if (within_word) {
                // word has ended
                within_word = false;
            }
        }
        else {
            if (! within_word) {
                // starting a new word
                word_ctr++;
                // and we're now within a word
                within_word = true;
            }
        }

    }
    cout << "The string \"" << text << "\" has " << word_ctr << " word";
    // pluralize word, is necessary
    if (word_ctr > 1) {
        cout << "s";
    }
    cout << "." << endl;
}

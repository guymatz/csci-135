#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int n, k;
    cout << "Please enter n: ";
    cin >> n;
    cout << "Please enter k: ";
    cin >> k;

    cout << pow( (1+n), k) / sqrt(k+1);

    return 0;
}

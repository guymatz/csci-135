#include <iostream>

using namespace std;

class ReverseUniverse {
    public:
        string reverseString(string s);
        int negate(int n);
        string aboutFace(string direction);
};

string ReverseUniverse::reverseString(string s) {
    string news = "";

    for (int i=s.length() ; i >= 0 ; i--) {
        news += s[i];
    }
    return news;
}


int main(){   
    ReverseUniverse r = ReverseUniverse();
    cout << r.reverseString("Hello23") << endl;
}

#include <iostream>
#include <vector>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> badCompany(vector<vector<int>> matrix, int k);
vector<int> sort_soldiers(vector<int> row, int k);
int count_soldiers(vector<int> row);

int main(){   
    vector<int> r1 = {1, 1, 0, 0, 0};
    vector<int> r2 = {1, 1, 1, 1, 0};
    vector<int> r3 = {1, 0, 0, 0, 0};
    vector<int> r4 = {1, 1, 0, 0, 0};
    vector<int> r5 = {1, 1, 1, 1, 1};
    vector<vector<int>> m = {r1, r2, r3, r4, r5};

    vector<int> n = badCompany(m, 3);
    for (int i=0; i < n.size(); i++) {
        cout << n[i] << endl;
    }
    return 0;
}

vector<int> sort_soldiers(vector<int> row, int k) {
    vector<int> count = {};
    for (int i=0; i <= row.size(); i++) {
        for (int j=0; j < row.size(); j++) {
            //cout << j << " " << row[j] << endl;
            if (row[j] == i) {
                //cout << j << endl;
                count.push_back(j);
            }
            if (count.size() == k) {
                break;
            }
        }
        if (count.size() == k) {
            break;
        }
    }
    return count;
}

int count_soldiers(vector<int> row) {
    int count = 0;
    for (int i=0; i < row.size(); i++) {
        if (row[i] == 1) {
            count++;
        }
    }
    return count;
}

vector<int> badCompany(vector<vector<int>> matrix, int k) {

    vector<int> soldiers = {};
    vector<int> k_sorted_soldiers = {};
    for (int i=0; i < matrix.size(); i++) {
        soldiers.push_back(count_soldiers(matrix[i]));
    }

    k_sorted_soldiers = sort_soldiers(soldiers, k);
    return k_sorted_soldiers;
}

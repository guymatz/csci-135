#include <iostream>
#include <vector>
#include <cmath>
#include <string>

using namespace std;

bool isPalindrome(string s);

class ReverseUniverse {
    public:
        string reverseString(string s);
        vector<double> reverseVector(vector<double> v);
        int reverseInt(int n);
};

int ReverseUniverse::reverseInt(int num) {
    string mun = "";
    int ctr = 0;
    int digit = 0;
    int power = 0;
    do {
        power = (int)pow(10, ctr+1);
        digit = num % power;
        //cout << digit << " " << ctr+1 << " " << power << endl;
        cout << "Num: " << num << " " << (int)pow(10, ctr+1) << endl;
        num -= digit;
        mun += digit /(int)pow(10, ctr); 
        cout << "Mun: " <<  int(digit /(int)pow(10, ctr)) << endl;
        cout << "mun: " << mun << endl;
        ctr++;
    } while (num > pow(10, ctr-1));
    return stoi(mun);
}

string ReverseUniverse::reverseString(string s) {
    string news = "";

    for (int i=s.length() ; i >= 0 ; i--) {
        news += s[i];
    }
    return news;
}

vector<double> ReverseUniverse::reverseVector(vector<double> v) {
    vector<double> newv;
    for (int i=v.size()-1; i>=0;  i--) {
        newv.push_back(v[i]);
    }
    return newv;
}

int main(){   
    ////q1
    ReverseUniverse r = ReverseUniverse();
    cout << r.reverseString("Hello23") << endl;
    ////q2
    vector<double> v = { 1,2,3,4,9,8,7,0};
    vector<double> v2 = r.reverseVector(v);
    cout << "size: " << v2.size() << endl;
    for (int i=0; i < v2.size(); i++) {
        cout << v2[i] << " " ;
    }
    cout << endl;
    ////q3
    //cout << r.reverseInt(136734);
    // q4
    cout << isPalindrome("honda civic") << endl;
    cout << isPalindrome("madamimadam") << endl;
    cout << isPalindrome("madammadam") << endl;

}

bool isPalindrome(string s) {
    for (int i=0; i < (s.length()/2); i++) {
        cout << s[i] << " " <<  s[s.length() - 1 - i ] << endl;
        if (s[i] != s[s.length() -1 - i]) {
            return false;
        }
    }
    return true;
}

#include <iostream>
#include <vector>

using namespace std;

vector<string> tokenize(string sentence);
int isPrefix(string sentence, string searchWord);

int main() {
    string text = "this small problemo is an easy problemo";

    cout << isPrefix(text, "prob") << endl;
    return 0;
}

int isPrefix(string sentence, string searchWord) {
    vector<string> words = {};
    words = tokenize(sentence);
    
    string word = "";
    for (int i=0; i < words.size(); i++) {
        word = words[i];
        cout << "word: " << word << endl;
        for (int j=0; (j < searchWord.length()); j++) {
            if (searchWord[j] != word[j]) {
                // cout << "NO!: " << searchWord[j] << " != " << word[j] << endl;
                break;
            }
            else if (j == searchWord.length()-1) {
                return i;
            }
        }
    }
    return -1;
}

vector<string> tokenize(string sentence) {
    // split up words into a vector
    vector<string> words = {};
    string word = "";
    bool within_word = false;
    for (int i=0; i < sentence.length(); i++) {
        if (sentence[i] == ' ' or (i == sentence.length()-1) ) {
            if (within_word) {
                // word has ended
                within_word = false;
                words.push_back(word);
                word = "";
            }
        }
        else if (! within_word) {
            // starting a new word
            word += sentence[i];
            // and we're now within a word
            within_word = true;
        }
        else {
            word += sentence[i];
        }

    }
    /*
    for (int i=0; i < words.size(); i++) {
        cout << words[i] << endl;
    }
    */

    return words;
}

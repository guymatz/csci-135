#include <iostream>
#include <vector>

using namespace std;

class ReverseUniverse {
    public:
        string reverseString(string s);
        vector<double> reverseVector(vector<double> v);
        int reverseInt(int n);
};

string ReverseUniverse::reverseString(string s) {
    string news = "";

    for (int i=s.length() ; i >= 0 ; i--) {
        news += s[i];
    }
    return news;
}

vector<double> ReverseUniverse::reverseVector(vector<double> v) {
    vector<double> newv;
    for (int i=v.size()-1; i>=0;  i--) {
        newv.push_back(v[i]);
    }
    return newv;
}

int main(){   
    ReverseUniverse r = ReverseUniverse();
    cout << r.reverseString("Hello23") << endl;
    vector<double> v = { 1,2,3,4,9,8,7,0};
    vector<double> v2 = r.reverseVector(v);
    cout << "size: " << v2.size() << endl;
    for (int i=0; i < v2.size(); i++) {
        cout << v2[i] << " " ;
    }
    cout << endl;

}

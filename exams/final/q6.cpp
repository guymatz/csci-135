Q6
10 Points

6.1. What is Object-oriented Programming (OOP)? How do we implement OOP in C++? (3 points)
OOP is a programming model which allows for the encapsulation of methods and data.  In C++, OOP is implemented by defining classes which contain data and the implementations of methods to at upon that data.

6.2. What is the difference between public area and private area in a class? (3 points)
The public area of a class is accessible from outside of the class.  The private area is not.

6.3. What are objects in C++? (2 points)
An object is an instance of a class.

6.4. Why do we need separate compilation in programming? (2 points)
Separate compilation allows for easier code organization.

10 Points

5.1. Please give an example of allocating dynamic memory in C++. (3 points)
array* arr = new array

5.2. Why do we need to deallocate dynamic memory? (2 points)
So that the memory can be used again

5.3. Please deallocate the dynamic memory you allocated in Q5.1. (3 points)
delete arr

5.4. Please demonstrate how to prevent dangling pointer for the pointer you used in Q5.3. (2 points)
arr = nullptr

#include <iostream>
#include <vector>
#include <cmath>
#include <string>

using namespace std;

bool isPalindrome(string s);

class ReverseUniverse {
    public:
        string reverseString(string s);
        vector<double> reverseVector(vector<double> v);
        int reverseInt(int n);
};

// r.reverseInt(136734);
// r.reverseInt(734);
int ReverseUniverse::reverseInt(int num) {
    int mun = 0;  // reverse num
    int ctr = 0;
    int digit = 0;
    int power = 0;
    while (num > pow(10, ctr)) {
        mun *= 10;
        power = (int)pow(10, ctr+1);
        digit = (num % power)/(power/10);
        num = num - digit;
        mun = mun + digit;
        ctr++;
    }

    return mun;
}

string ReverseUniverse::reverseString(string s) {
    string news = "";

    for (int i=s.length() ; i >= 0 ; i--) {
        news += s[i];
    }
    return news;
}

vector<double> ReverseUniverse::reverseVector(vector<double> v) {
    vector<double> newv;
    for (int i=v.size()-1; i>=0;  i--) {
        newv.push_back(v[i]);
    }
    return newv;
}

int main(){   
    ////q1
    ReverseUniverse r = ReverseUniverse();
    /*
    cout << r.reverseString("Hello23") << endl;
    */
    ////q2
    /*
    vector<double> v = { 1,2,3,4,9,8,7,0};
    vector<double> v2 = r.reverseVector(v);
    cout << "size: " << v2.size() << endl;
    for (int i=0; i < v2.size(); i++) {
        cout << v2[i] << " " ;
    }
    cout << endl;
    */
    ////q3
    cout << 734 << " " << r.reverseInt(734) << endl;
    cout << 136734 << " " << r.reverseInt(136734) << endl;
    cout << 367341 << " " << r.reverseInt(367341) << endl;
    // q4
    /*
    cout << isPalindrome("honda civic") << endl;
    cout << isPalindrome("civic") << endl;
    */

}

bool isPalindrome(string s) {
    for (int i=0; i < (s.length()/2); i++) {
        cout << s[i] << " " <<  s[s.length() - 1 - i ] << endl;
        if (s[i] != s[s.length() -1 - i]) {
            return false;
        }
    }
    return true;
}

#include <iostream>
using namespace std;

int main(){
    int m = 3;
    int n = 2;
    int *p = &m; // p points to the memory addrss of m
    cout << *p << endl;  //  prints out conetns of p -> m, 3
    p = &n; //  now p points to n
    cout << *p << endl;  // prints out 2
    m = *p; // m = 2
    cout << m << endl; //2
}

#include <iostream>
#include <vector>

using namespace std;

void swap(vector<int>& nums);

int main() {
    vector<int> nums = {9, 13, 21, 4, 11, 7, 1, 3};
    for (int i=0; i < nums.size(); i++) {
        cout << nums[i] ;
    }
    cout << endl;
    swap(nums);
    for (int i=0; i < nums.size(); i++) {
        cout << nums[i] ;
    }
    cout << endl;
}

void swap(vector<int>& nums) {

    int tmp;

    for (int i=0; i < nums.size()/2; i++) {
        tmp = nums[i];
        nums[i] = nums[nums.size()/2 + i];
        nums[nums.size()/2 + i] = tmp;
    }
}


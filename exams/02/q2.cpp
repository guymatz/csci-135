#include <iostream>
#include <vector>

using namespace std;

vector<int> unduplicate(vector<int> nums);

int main() {
    vector<int> nums = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
    vector<int> nnums = {};
    nnums = unduplicate(nums);
    for (int i=0; i < nnums.size(); i++) {
        cout << nnums[i] << endl;
    }
}

vector<int> unduplicate(vector<int> nums) {

    vector<int> new_nums = {nums[0]};
    int latest_greatest_num = nums[0];

    for (int i=1; i < nums.size(); i++) {
        if (nums[i] > latest_greatest_num) {
            new_nums.push_back(nums[i]);
            latest_greatest_num = nums[i];
        }
    }
    return new_nums;
}


#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    const int ROWS = 5;
    const int COLUMNS = 5;
    int lucky[ROWS][COLUMNS];

    for (int r=0; r < ROWS; r++) {
        for (int c=0; c < COLUMNS; c++) {
            lucky[r][c] = r * c;
        }
    }

    for (int r=0; r < ROWS; r++) {
        for (int c=0; c < COLUMNS; c++) {
            cout << setw(3) << lucky[r][c];
        }
        cout << endl;
    }
}

#include <iostream>

using namespace std;

class Rectangle {
public:
    int height;
    int width;
    int area();
    int perimeter();
};

int Rectangle::area() {
    return height * width;
}

int Rectangle::perimeter() {
    return (2 * height) + (2 * width);
}

int main() {

    int height, width;
    cout << "Enter height and width: ";
    cin >> height >> width;
    Rectangle r = {height, width};
    cout << "Area: " << r.area() << endl;
    cout << "Perimeter: " << r.perimeter() << endl;
    
}

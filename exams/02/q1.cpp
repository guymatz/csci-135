#include <iostream>
#include <cmath>

using namespace std;

void swap(int& a, int& b){
    int temp = a;
    a = b;
    b = temp;
}

int main(){   
    int m = 2;
    int n = 3;
    cout << "Before: " << m << " " << n << endl;
    swap(m, n);
    cout << "After: " << m << " " << n << endl;
}

// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 5 - increaing, decreasing, neither?
*/

#include <iostream>
using namespace std;

int main() {

    int nums[3];
    cout << "input: ";
    for (int i=0; i<3; i++) {
        cin >> nums[i];
    }

    cout << "output: ";
    // here's a comment
    // here's another comment
    if (nums[0] > nums[1] && nums[1] > nums[2]) {
        cout << "decreasing" << endl;
    }
    else if (nums[0] < nums[1] && nums[1] < nums[2]) {
        cout << "increasing" << endl;
    }
    else {
        cout << "neither" << endl;
    }
}



// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 5 - increaing, decreasing, neither?
*/

#include <iostream>
using namespace std;

string middle(string);

int main() {

    cout << middle("abcde") << endl;
    cout << middle("hello") << endl;
    cout << middle("food") << endl;
}

string middle(string text) {
    string middle = "";

    int middle_pos = text.length()/2;
    if (text.length() % 2 == 0) {
        middle += text[middle_pos-1];
        middle += text[middle_pos];
        return middle;
    }
    else {
        middle = text[middle_pos];
        return middle;
    }
}

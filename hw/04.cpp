// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * Ask for a word, print it out vertically
*/

#include <iostream>
using namespace std;

int main() {

    int word;
    cout << "Number, please: ";
    cin >> word;

    // here's a comment
    // here's another comment
    if (word > 0) {
        cout << "positive" << endl;
    }
    else if (word < 0) {
        cout << "negative" << endl;
    }
    else {
        cout << "zero" << endl;
    }
}



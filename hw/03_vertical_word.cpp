// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * Ask for a word, print it out vertically
*/

#include <iostream>
using namespace std;

int main() {

    string word;
    cout << "Word, please: ";
    cin >> word;

    // here's a comment
    // here's another comment
    for (int i=0; i < word.length(); i++) {
        cout << word[i] << endl;
    }
}



// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 10 - function that receives two pointers and sorts 
 * the values to which they point
*/

#include <iostream>
#include <cmath>
using namespace std;

class Rectangle {
    private:
        float width, height;
    public:
        float get_perimeter();
        float get_area();
        void resize(double factor);
        Rectangle(float w, float h);
};

Rectangle::Rectangle(float w, float h) {
    width = w;
    height = h;
}

float Rectangle::get_perimeter() {
    return ((2 * width) + (2 * height));
}

float Rectangle::get_area() {
    return width * height;
}

void Rectangle::resize(double factor) {
    height *= factor;
    width *= factor;
}

int main() {
  
    float w, h;
    cout << "Width : ";
    cin >> w;
  
    cout << "Height : ";
    cin >> h;
  
    Rectangle r = Rectangle(w, h);
    cout << "Perimeter " << r.get_perimeter() << endl;
    cout << "Area " << r.get_area() << endl;
    r.resize(3);
    cout << "Perimeter " << r.get_perimeter() << endl;
    cout << "Area " << r.get_area() << endl;
    
    return 0;
}

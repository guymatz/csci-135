// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 5 - increaing, decreasing, neither?
*/

#include <iostream>
using namespace std;
void sort2(int& a, int& b);

int main() {

    int i=3; int j=2;
    sort2(i,j);
    cout << "i: " << i << ", j: " << j << endl;

    i=2; j=3;
    sort2(i,j);
    cout << "i: " << i << ", j: " << j << endl;
}

void sort2(int& a, int &b) {
    if (a > b) {
        int t = b;
        b = a;
        a = t;
    }

    return;
}

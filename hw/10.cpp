// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 10 - function that receives two pointers and sorts 
 * the values to which they point
*/

#include <iostream>
using namespace std;

void sort2(double* a, double* b) {
  double tmp = 0;
  if (*a > *b) {
    tmp = *b;
    *b = *a;
    *a = tmp;
  }
}

int main() {
  double x = 7;
  double y = 2;
  sort2(&x, &y);
  cout << x << " " << y << endl;

  x = 2;
  y = 7;
  sort2(&x, &y);
  cout << x << " " << y << endl;
  
  return 0;
}

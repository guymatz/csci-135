// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 10 - function that receives two pointers and sorts 
 * the values to which they point
*/

#include <iostream>
#include <cmath>
using namespace std;

struct Point {
    double x;
    double y;
};

double distance(Point a, Point b);

int main() {
  
    double p1x, p1y, p2x, p2y;
    cout << "Point 1: ";
    cin >> p1x >> p1y;
  
    cout << "Point 2: ";
    cin >> p2x >> p2y;

    Point p1 = {p1x, p1y};
    Point p2 = {p2x, p2y};

    cout << distance(p1, p2) << endl;
    
    return 0;
}

double distance(Point a, Point b) {
    return sqrt( pow((a.x - b.x), 2) + pow((a.y - b.y), 2));
}

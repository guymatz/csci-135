// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 10 - function that receives two pointers and sorts 
 * the values to which they point
*/

#include <iostream>
#include <cmath>
using namespace std;

struct Point {
    double x;
    double y;
};

struct Triangle {
    Point p1, p2, p3;
};

double distance(Point a, Point b);
double perimeter(Triangle t);

int main() {
  
    double p1x, p1y, p2x, p2y, p3x, p3y;
    cout << "Point 1: ";
    cin >> p1x >> p1y;
  
    cout << "Point 2: ";
    cin >> p2x >> p2y;
  
    cout << "Point 3: ";
    cin >> p3x >> p3y;

    Point p1 = {p1x, p1y};
    Point p2 = {p2x, p2y};
    Point p3 = {p3x, p3y};

    Triangle t = {p1, p2, p3};
    cout << perimeter(t) << endl;
    
    return 0;
}

double distance(Point a, Point b) {
    return sqrt( pow((a.x - b.x), 2) + pow((a.y - b.y), 2));
}

double perimeter(Triangle t) {
    double side_a = distance(t.p1, t.p2);
    //cout << "Side A: " << side_a << endl;
    double side_b = distance(t.p2, t.p3);
    //cout << "Side B: " << side_b << endl;
    double side_c = distance(t.p3, t.p1);
    //cout << "Side C: " << side_c << endl;

    return side_a + side_b + side_c;
}

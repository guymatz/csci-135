// Here's a comment
/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: HW 2
Write a program that asks the user to input:
  1. the number of gallons og gas in the tank
  2. the fuel efficiency in miles per gallon
  3. the proce of gas per gallon
Then print the cost per 100 miles and how far the car can go with the gas
in the tank

*/

#include <iostream>
#include <cmath>
using namespace std;

int main() {

    float gallons_in_tank, mpg, gas_price;
    cout << "Please enter the number of gallons of gas in the tank: ";
    cin >> gallons_in_tank;
    cout << "Please enter the fuel efficiency in miles per gallon: ";
    cin >> mpg;
    cout << "Please enter the price of gas per gallon: ";
    cin >> gas_price;

    cout << "Cost per 100 miles: " << ( 100 / mpg ) * gas_price << endl;
    cout << "The car can travel: " << gallons_in_tank * mpg << endl;
    return 0;
}


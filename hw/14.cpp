// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 10 - function that receives two pointers and sorts 
 * the values to which they point
*/

#include <iostream>
#include <vector>
using namespace std;

vector<int> merge_sorted(vector<int> a, vector<int>b);


int main() {
    vector<int> a = {1,4,9,16};
    vector<int> b = {4,7,9,9,11};

    vector<int> c = merge_sorted(a,b);
    for (int i=0; i < c.size(); i++) {
        cout << c[i]  << " " ;
    }
    return 0;
}


vector<int> merge_sorted(vector<int> a, vector<int>b) {
    vector<int> merged = {};

    int a_idx = 0;
    int b_idx = 0;

    while (a_idx < a.size() and b_idx < b.size()) {
        //cout << "in merged " << " " << a_idx << " " << b_idx << ": ";
        for (int i=0; i < merged.size(); i++) {
            //cout << merged[i]  << " " ;
        }
        //cout << endl;
        if (a[a_idx] < b[b_idx]) {
            merged.push_back(a[a_idx]);
            a_idx++;
        }
        else {
            merged.push_back(b[b_idx]);
            b_idx++;
        }
    }
    //cout << a_idx << " " << a.size() << " " << b_idx  << " " << b.size() << endl;
    if ( a_idx == (a.size() )) {
        for (int i=b_idx; i< b.size(); i++) {
            merged.push_back(b[i]);
            //cout << b[i]  << " " ;
        }
    }
    else {
        for (int i=a_idx; i< a.size(); i++) {
            merged.push_back(a[i]);
            //cout << a[i]  << " " ;
        }
    }
    return merged;
}

// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 5 - increaing, decreasing, neither?
*/

#include <iostream>
using namespace std;
void sort3(int& a, int& b, int & c);

int main() {

    int i=3; int j=2; int k=4;
    sort3(i,j, k);
    cout << "i: " << i << ", j: " << j << ", k: " << k << endl;

    i=2; j=3; k=1;;
    sort3(i,j, k);
    cout << "i: " << i << ", j: " << j << ", k: " << k << endl;
}

void sort3(int& a, int &b, int & c) {
    if (a > b && a > c) {
        int t = c;
        c = a;
        a = t;
    }
    if (b > c) {
        int t = c;
        c = b;
        b = t;
    }
    if (a > b) {
        int t = b;
        b = a;
        a = t;
    }
    return;
}

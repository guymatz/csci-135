// Here's a comment
/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: HW 1, Write a program that prints three items, such as the names
of your 3 best friends or favorite movies, on three separate lines

*/

#include <iostream>
#include <string>
using namespace std;

int main() {

    string name1, name2, name3;
    cout << "Please enter a name: ";
    cin >> name1;
    cout << "Please enter a name: ";
    cin >> name2;
    cout << "Please enter a name: ";
    cin >> name3;
    cout << name1 << endl;
    cout << name2 << endl;
    cout << name3 << endl;

    return 0;
}


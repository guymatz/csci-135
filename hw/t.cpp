#include <iostream>
using namespace std;

int main() {
    int m = 10;
    int n = 20;
    int* p = &m;

    cout << p << endl;
    p = &n;
    cout << *p << endl;

    return 0;
}

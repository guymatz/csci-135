// here's an initial comment
/*
 * Guy Matz
 * CSCI - 136
 * HW 9 - function that check whether two arrays are the same
*/

#include <iostream>
using namespace std;
bool equals(int a[], int a_size, int b[], int b_size);

int main() {

    int a[3] = {1,2,3};
    int b[3] = {1,2,3};
    cout << equals(a, 3, b, 3) << endl;

    int c[3] = {4,5,6};
    int d[3] = {4,5};
    cout << equals(c, 3, d, 2) << endl;

    int e[3] = {90,80,100};
    int f[3] = {1,2,3};
    cout << equals(e, 3, f, 3) << endl;
}

bool equals(int a[], int a_size, int b[], int b_size) {
    if (a_size != b_size) {
        return false;
    }
    
    for (int i=0; i < a_size; i++) {
        if (a[i] != b[i]) {
            return false;
        }
    }
    return true;
}

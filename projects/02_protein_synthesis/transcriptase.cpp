/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: project 2

write a program called transcriptase.cpp that reads a text file called
dna.txt that contains one DNA strand per line and outputs to the console
the corresponding mRNA strands. 
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

char DNAbase_to_mRNAbase(char n);
string DNA_to_mRNA(string strand);

int main() {
    string data_file = "dna.txt";
    
    ifstream fin(data_file);
    if (fin.fail()) {
        cerr << "File caanot ve read, or something\n";
        exit(1);
    }
    string strand;
    while (getline(fin, strand)) {
        cout << DNA_to_mRNA(strand) << endl;
    }
    fin.close();
}

char DNAbase_to_mRNAbase(char n) {
    n = toupper(n);
    if (n == 65) return n+20;
    else if (n == 67) return n+4;
    else if (n == 71) return n-4;
    else if (n == 84) return n-19;
    else return n;
}
        
string DNA_to_mRNA(string strand) {
    string sartnd="";
    for (int i=0; i < strand.size(); i++) {
        sartnd += DNAbase_to_mRNAbase(strand[i]);
    }
    return sartnd;
}

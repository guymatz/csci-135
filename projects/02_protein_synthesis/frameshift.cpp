/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: project 2c

For each pair in mutations.txt, output to the console the Hamming distance
followed by “yes” or “no” whether the substitution caused a change in
structure. 
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

char DNAbase_to_mRNAbase(char n);
string DNA_to_mRNA(string strand);
string amino_lookup(ifstream &dict, string codon_in);
string dna_to_amino(string strand);
bool is_stop_codon(string mrna);
int hamming(string strand_1, string strand_2);

int main() {
    string data_file = "frameshift_mutations.txt";
    string codon_file = "codons.tsv";
    const string START = "AUG";
    
    ifstream fin(data_file);
    if (fin.fail()) {
        cerr << "File caanot be read, or something\n";
        exit(1);
    }
    string strand_1;
    string strand_2;
    int hscore = 0;
    while (getline(fin, strand_1)) {
        getline(fin, strand_2);
        /*
        hscore = hamming(strand_1, strand_2) ;
        */
        /*
        cout << strand_1 << endl;
        cout << strand_2 << endl;
        cout << DNA_to_mRNA(strand_1) << endl;
        cout << DNA_to_mRNA(strand_2) << endl;
        */
        cout << dna_to_amino(strand_1) << endl;
        cout << dna_to_amino(strand_2) << endl;
        /*
        cout << hscore << " ";
        if (dna_to_amino(strand_1) == dna_to_amino(strand_2)) {
            cout << "no" << endl;
        }
        else {
            cout << "yes" << endl;
        }
        */
    }
    fin.close();
}

string dna_to_amino(string strand) {

    ifstream data("codons.tsv");
    string amino,aminos,codon;
    string dna,mrna;
    bool started = false;
    int check_length=1;
    for (int i=0; i < strand.length(); i=i+check_length) {
        // cout << "i = " << i << ", i+3 = " << i +3 << endl;
        //cout << "Amino String : " << aminos << endl;
        dna = strand.substr(i, 3);
        //cout << "dna = " << dna << endl;
        mrna = DNA_to_mRNA(dna);
        //cout << "Checking " << mrna << endl;
        if (is_stop_codon(mrna) and started) {
            //cout << "Stopping on : " << mrna << endl;
            break;
        }
        //cout << "Looking for amino for " << mrna << endl;
        amino = amino_lookup(data, mrna);
        //cout << "Found amino " << amino << endl;
        if (started and (amino != "")) {
            aminos = aminos + "-" + amino;
        }
        else if (not started and amino == "Met") {
            //cout << "Started with : " << mrna << endl;
            aminos = amino;
            started = true;
            check_length = 3;
        }
        else {
            continue;
        }
    }
    return aminos;
}

int hamming(string strand_1, string strand_2) {
    int score = 0;
    for (int i=0; i < strand_1.length(); i++) {
        if (strand_1[i] != strand_2[i]) {
            score++;
        }
    }
    return score;
}

bool is_stop_codon(string mrna) {
    if (mrna == "UAA" or mrna == "UGA" or mrna == "UAG") {
        return true;
    }
    else {
        return false;
    }
}


string amino_lookup(ifstream &dict, string codon_in) {
    string key, value;
    dict.clear(); // reset error state
    dict.seekg(0); // return file pointer to the beginning
    while (dict >> key >> value) {
        // cout << "Comparing " << key << ":" << codon_in << endl;
        if (key == codon_in) {
            return value;
        }
    }
    // cout << "Could not find amino for " << codon_in << endl;
    return "";
}

char DNAbase_to_mRNAbase(char n) {
    n = toupper(n);
    if (n == 65) return n+20;
    else if (n == 67) return n+4;
    else if (n == 71) return n-4;
    else if (n == 84) return n-19;
    else return n;
}
        
string DNA_to_mRNA(string strand) {
    string sartnd="";
    for (int i=0; i < strand.size(); i++) {
        sartnd += DNAbase_to_mRNAbase(strand[i]);
    }
    return sartnd;
}

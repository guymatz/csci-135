/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: project 2b

given strands of DNA (taken from dna2b.txt), outputs to the console the
corresponding amino-acid chain
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
using namespace std;

char DNAbase_to_mRNAbase(char n);
string DNA_to_mRNA(string strand);
string amino_lookup(ifstream &dict, string codon_in);
bool is_stop_codon(string mrna);

int main() {
    string data_file = "dna2b.txt";
    string codon_file = "codons.tsv";
    const string START = "AUG";
    
    ifstream fin(data_file);
    ifstream data(codon_file);
    if (fin.fail()) {
        cerr << "File caanot be read, or something\n";
        exit(1);
    }
    string strand;
    while (getline(fin, strand)) {
        // cout << DNA_to_mRNA(strand) << endl;
        string amino,aminos,codon;
        string dna,mrna;
        bool started = false;
        for (int i=0; i < strand.length(); i=i+3) {
            // cout << "i = " << i << ", i+3 = " << i +3 << endl;
            dna = strand.substr(i, 3);
            // cout << "dna = " << dna << endl;
            mrna = DNA_to_mRNA(dna);
            if (is_stop_codon(mrna)) {
                    break;
            }
            //cout << "Looking for amino for " << mrna << endl;
            amino = amino_lookup(data, mrna);
            //cout << "Found amino " << amino << endl;
            if (started) {
                aminos = aminos + "-" + amino;
            }
            else if (not started and amino == "Met") {
                aminos = amino;
                started = true;
            }
            else {
                continue;
            }
        }
        cout << aminos << endl;
    }
    fin.close();
}

bool is_stop_codon(string mrna) {
    if (mrna == "UAA" or mrna == "UGA" or mrna == "UAG") {
        return true;
    }
    else {
        return false;
    }
}


string amino_lookup(ifstream &dict, string codon_in) {
    string key, value;
    dict.clear(); // reset error state
    dict.seekg(0); // return file pointer to the beginning
    while (dict >> key >> value) {
        // cout << "Comparing " << key << ":" << codon_in << endl;
        if (key == codon_in) {
            return value;
        }
    }
    cout << "Could not find amino for " << codon_in;
    return "poopy";
}

char DNAbase_to_mRNAbase(char n) {
    n = toupper(n);
    if (n == 65) return n+20;
    else if (n == 67) return n+4;
    else if (n == 71) return n-4;
    else if (n == 84) return n-19;
    else return n;
}
        
string DNA_to_mRNA(string strand) {
    string sartnd="";
    for (int i=0; i < strand.size(); i++) {
        sartnd += DNAbase_to_mRNAbase(strand[i]);
    }
    return sartnd;
}

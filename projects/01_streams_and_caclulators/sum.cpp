/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: project 1

Write a program sum.cpp that reads a sequence of integers from cin,
and reports their sum
*/

#include <iostream>
using namespace std;

int main() {
    int num_in = 0;
    
    int sum = 0;
    while (cin >> num_in ) {
        sum += num_in;
    }

    cout << sum;

    return 0;
}

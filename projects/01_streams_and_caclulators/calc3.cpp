/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: project 1

Write a program sum.cpp that reads a sequence of integers from cin,
and reports their sum
*/

#include <iostream>
#include <cctype>
using namespace std;

int main() {
  char operator_in, operator_prev;
  int number_in = 0;
  int number_prev = 0;
  int total = 0;

  operator_in = '+';
  while (cin >> number_in) {
      operator_prev = operator_in;
      //cout << "np: " << number_prev << endl;
      //cout << "ni: " << number_in << endl;
      //cout << "op: " << operator_prev << endl;
      //cout << "oi: " << operator_in << endl;
      cin >> operator_in;
      if (operator_in == '^') {
          number_in *= number_in;
          //cout << "am I here ^ ***" << number_in << endl;;
          cin >> operator_in;
          //cout << " op in " << operator_in << endl;;
      }

      if (operator_in == ';') {
          //cout << "am I here ; ******" << endl;;
          if (operator_prev == '+') {
              total += number_in;
              //cout << "am I here + ******" << endl;;
          }
          if (operator_prev == '-') {
              total -= number_in;
              //cout << "am I here - ******" << endl;;
          }
          //cout << "Line total: " << total << endl;
          cout << total << endl;
          total = 0;
          number_in = 0;
          number_prev = 0;
          operator_in = '+';
          operator_prev = '+';
          continue;
      }
    
      if (operator_prev == '+') {
          total += number_in;
          //cout << "am I here + ???? " << total << " opin " << operator_in << endl;
          continue;
      }

      if (operator_prev == '-') {
          //cout << "am I here - ????? 1 " << total <<  " opin " << operator_in << endl;
          total -= number_in;
          //cout << "am I here????? 2 " << total << endl;
          continue;
      }

  }
  return 0;
}

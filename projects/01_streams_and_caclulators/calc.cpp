/*
Author: Guy Matz
Course: CSCI-136
Instructor: Minh Nguyen
Assignment: project 1

Write a program sum.cpp that reads a sequence of integers from cin,
and reports their sum
*/

#include <iostream>
using namespace std;

int main() {
    char operator_in;
    int number_in;
    int total = 0;
    
    cin >> number_in;
    total = total + number_in;
    while (cin >> operator_in >> number_in) {
        // cout << " " << number_in << " " << operator_in;
        if (operator_in == '+') {
            total += number_in;
        }
        else {
            total -= number_in;
        }
    }

    cout << total;

    return 0;
}
